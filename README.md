It's less than a year until the Olympic Summer Games in Tokio. Once again,
the "Youth of the World" meets to compete for gold and set new records. Just how
youthful are the athletes, though? Undoubtly, young champions make for the most
talked about headlines. We are going to look at how big of an exception they are
and in which sports athletes aged 30+ are still competitive.
To keep results comparable, we are limiting ourselves on the medal winners since 1984.
There are two reasons for this:

1. The Olympics have changed a lot since the first modern Games in 1896. 
This applies both to the sports and the athletes competing in it. Since 1984, 
when they were officially opened to professional athletes, things have stayed 
mostly the same.

2. Under certain circumstances it is fairly easy to participate in the Games.
To ensure a certain standard of competitiveness among all considered athletes,
it seems reasonable to exclude non-medalists.


First, a look at the median age of all medalists in the modern Olympic Games.

![Median Age all medalists](/Agetrend.jpeg)

Given that men and women each compete in the same events, it is somewhat surprising
to see that male medalists are often around two years older. With the available
data it unfortunately impossible to say anything about the reasons for this difference.

The small margins by which competions at this level are often won might lead 
some to believe that being slightly younger gives a small but decisive advantage.
This does not appear to be the case, though. We can also see that only very few 
medalists are still teenagers. However, quite a few of them are already older 
than 30 years.
![Agedistribution](/agedistribution.jpeg)


In which sports do these people primarily compete?

![Agerange medalists](/agerange.jpeg)

*To be continued*